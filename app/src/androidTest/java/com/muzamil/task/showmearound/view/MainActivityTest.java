package com.muzamil.task.showmearound.view;

import android.support.test.espresso.action.ViewActions;
import android.test.ActivityInstrumentationTestCase2;

import com.muzamil.task.showmearound.R;
import com.muzamil.task.showmearound.utility.VenueListDTO;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * Created by Muzamil.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mActivity;
    private String searchQuery;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        //get instance of activity at start
        mActivity = getActivity();
        searchQuery = "Oulu";// string to search
    }

    /*To verify that if network in disable then application should not get crashed and show proper error message
    / prerequisite: Network or any data package should be disabled
    */

    public void testNetworkErrorMessage() {
        //type text in search view
        onView(withId(R.id.item_searchVenue)).perform(ViewActions.typeText("oulu"));
        //verify the text is displayed
        onView(withId(R.id.item_tvErrorReport)).check(matches(isDisplayed()));
        //verify error message
        onView(withId(R.id.item_tvErrorReport)).check(matches(withText(containsString("Error in network connection"))));
    }
    /*
    / To verify that if connections to foursquare fails (i.e. missing parameters or wrong query) then application should give proper error message
    / prerequisite: Wrong or missing credentials for query
    */

    public void testConnectionErrorMessage() {
        //type text in search view
        onView(withId(R.id.item_searchVenue)).perform(ViewActions.typeText(searchQuery));
        //verify error text view is visible on application view
        onView(withId(R.id.item_tvErrorReport)).check(matches(isDisplayed()));
        //verify error message
        onView(withId(R.id.item_tvErrorReport)).check(matches(withText(containsString("Connection with server failed, please try again later"))));
    }
    /*
    / To verify that application should not show any error message text view on successful response from foursquare
    / prerequisite: client id and secret should be placed in present correctly
   */

    public void testErrorTextViewShouldBeInvinsibleIfRequestProcessedSuccessfully() {
        //type text in search view
        onView(withId(R.id.item_searchVenue)).perform(ViewActions.typeText(searchQuery));
        // verify error text view is not displayed on application view
        onView(withId(R.id.item_tvErrorReport)).check(matches(not(isDisplayed())));
    }

	/*To verify that if location services is disabled then application should show alert dialog to enable location services
    / prerequisite: Location Service on Mobile should be disabled
    */

    public void testAlertDialogisDisplayedIfLocationisOffOnDevice() {
        //enter text in search view
        onView(withId(R.id.item_searchVenue)).perform(ViewActions.typeText(searchQuery));
        // wait for 6 second for alert dialog
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // verify alert dialog is visible on application view
        onView(withText("GPS Settings")).inRoot(isDialog()).check(matches(isDisplayed()));
    }

    /*
    / To verify that if application does not find any relevant venue with the query string then it should give proper error message
    / prerequisite: Application is connected to internet,client_id and secret key is placed correctly in Presenter.
   */

    public void testErrorTextMessageWhenNoRelevantVenueFoundAgainstSearhQuery() {
        //enter text in search view
        onView(withId(R.id.item_searchVenue)).perform(ViewActions.typeText("kjggljhhjn"));
        //verify error text view is visible on application view
        onView(withId(R.id.item_tvErrorReport)).check(matches(isDisplayed()));
        //verify error report
        onView(withId(R.id.item_tvErrorReport)).check(matches(withText(containsString("No relevant venue found against your search string..."))));
    }


    /*To verify that if user enter a search query in search view then application should show venue name relevant to that search query
    / prerequisite: Application is connected to internet,client_id and secret key is placed correctly in Presenter.
    */
    public void testListViewResultWithVenueName() {
        //enter search query in search view
        onView(withId(R.id.item_searchVenue)).perform(ViewActions.typeText(searchQuery));
        //verify search results in list view
        onData(allOf(is(instanceOf(VenueListDTO.class)))).inAdapterView(withId(R.id.item_listView)).atPosition(0).
                onChildView(withId(R.id.item_TvName)).check(matches(withText(containsString(searchQuery))));

    }

    /*To verify that if user enter a search query in search view then application should show distance of venues relevant to that query string
    / prerequisite: Application is connected to internet,client_id and secret key is placed correctly in Presenter.
    */
    public void testListViewResultDisplayDistance() {
        // enter search query in search view
        onView(withId(R.id.item_searchVenue)).perform(ViewActions.typeText(searchQuery));
        // verify if distance is displayed
        onData(allOf(is(instanceOf(VenueListDTO.class)))).inAdapterView(withId(R.id.item_listView)).atPosition(0).
                onChildView(withId(R.id.item_TvDistance)).check(matches(isDisplayed()));

    }

    /*To verify that if user enter a search query in search view then application should show address of venues relevant to that query string
    / prerequisite: Application is connected to internet,client_id and secret key is placed correctly in Presenter.
    */
    public void testListViewResultDisplayAddress() {
        // enter search query in search view
        onView(withId(R.id.item_searchVenue)).perform(ViewActions.typeText(searchQuery));
        // verify if address is displayed
        onData(allOf(is(instanceOf(VenueListDTO.class)))).inAdapterView(withId(R.id.item_listView)).atPosition(0).
                onChildView(withId(R.id.item_TvAddress)).check(matches(isDisplayed()));

    }

}