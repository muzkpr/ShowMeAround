package com.muzamil.task.showmearound.backend;

import com.muzamil.task.showmearound.model.MyHttpManager;
import com.muzamil.task.showmearound.utility.VenueListDTO;
import com.muzamil.task.showmearound.utility.VenueListJSONParsor;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Muzamil.
 */
public class MainBackendTest {

    private MyHttpManager myHttpManager;
    private final String baseQuery = "https://api.foursquare.com/v2/venues/search?"; //base query string from foursquare
    private final String clientID = ""; //client id
    private final String clientSecret = ""; //client secret
    private final int apiVersion = 20130815; // foursquare api version
    private double latitude = 0.0; //latitude
    private double longitude = 0.0;// longitude

    @Before
    public void setUp() throws Exception {
        myHttpManager = new MyHttpManager();
    }

    //Test on successful request, the response should be parsed and stored in DTOs ArrayList which can be iterate later.
    @Test
    public void testOnSuccessfulRequestReceievedJSONObjectParsedAndTransferedSuccessfullyInDataTransferObject() throws Exception {
        String searchQuery = "oulu";
        double testLatitude = 65.0581571;
        double testLongitude = 25.474109;
        String prepaidQuery = baseQuery + "client_id=" + clientID
                + "&client_secret=" + clientSecret + "&v=" + apiVersion
                + "&ll=" + testLatitude + "," + testLongitude + "&query=" + searchQuery;//prepare query for server request
        String respFromServer = MyHttpManager.getData(prepaidQuery);//try to get data from server and return as string object contain JSON data
        VenueListJSONParsor jp = new VenueListJSONParsor();
        //parse json object into ArrayList and sent to presenter for further processing.
        ArrayList<VenueListDTO> venueListDTOs = jp.parseResp(respFromServer);

        // verify name of first vanue
        assertEquals(venueListDTOs.get(0).getName(), "Oulun yliopisto / University of Oulu");
        // verify distance to first venue
        assertEquals(venueListDTOs.get(0).getDistance(), "349 Meters");
        // verify address of first venue
        assertEquals(venueListDTOs.get(0).getAddress(), "[\"Pentti Kaiteran Katu 1\",\"90570 Oulu\",\"Suomi\"]");
        // verify size of ArrayList as we are expecting 30 venues against our query
        assertEquals(venueListDTOs.size(), 30);

    }


    //Test handling error response from server in case GPS service failed to set latitude and longitude
    @Test
    public void testLocationParameterMissingErrorCodeResponseFromFoursquareServer() throws Exception {
        String searchQuery = "oulu";

        String prepaidQuery = baseQuery + "client_id=" + clientID
                + "&client_secret=" + clientSecret + "&v=" + apiVersion
                + "&ll=" + this.latitude + "," + this.longitude + "&query=" + searchQuery;//prepare query for server request
        String respFromServer = MyHttpManager.getData(prepaidQuery);//try to get data from server and return as string object contain JSON data
        VenueListJSONParsor jp = new VenueListJSONParsor();
        //parse json object into ArrayList and sent to presenter for further processing.
        ArrayList<VenueListDTO> venueListDTOs = jp.parseResp(respFromServer);

        // verify error code
        assertEquals(venueListDTOs.get(0).getErrorCode(), 400);
        // verify error type
        assertEquals(venueListDTOs.get(0).getErrorType(), "param_error");
        // verify error description
        assertEquals(venueListDTOs.get(0).getErrorDescription(), "Must provide parameter ll");

    }

    //Test handling error response from server in case of wrong secret id
    @Test
    public void testSecretMissingErrorCodeResponseFromFoursquareServer() throws Exception {
        String searchQuery = "oulu";

        String prepaidQuery = baseQuery + "client_id=" + clientID
                + "&client_secret=" + "&v=" + apiVersion
                + "&ll=" + this.latitude + "," + this.longitude + "&query=" + searchQuery;//prepare query for server request
        String respFromServer = MyHttpManager.getData(prepaidQuery);//try to get data from server and return as string object contain JSON data
        VenueListJSONParsor jp = new VenueListJSONParsor();
        //parse json object into ArrayList and sent to presenter for further processing.
        ArrayList<VenueListDTO> venueListDTOs = jp.parseResp(respFromServer);

        // verify error code
        assertEquals(venueListDTOs.get(0).getErrorCode(), 400);
        // verify error type
        assertEquals(venueListDTOs.get(0).getErrorType(), "invalid_auth");
    }

    //Test handling error response from server in case of wrong client id
    @Test
    public void testClientIdMissingErrorCodeResponseFromFoursquareServer() throws Exception {
        String searchQuery = "oulu";

        String prepaidQuery = baseQuery + "client_id="
                + "&client_secret=" + clientSecret + "&v=" + apiVersion
                + "&ll=" + this.latitude + "," + this.longitude + "&query=" + searchQuery;//prepare query for server request
        String respFromServer = MyHttpManager.getData(prepaidQuery);//try to get data from server and return as string object contain JSON data
        VenueListJSONParsor jp = new VenueListJSONParsor();
        //parse json object into ArrayList and sent to presenter for further processing.
        ArrayList<VenueListDTO> venueListDTOs = jp.parseResp(respFromServer);

        // verify error code
        assertEquals(venueListDTOs.get(0).getErrorCode(), 400);
        // verify error type
        assertEquals(venueListDTOs.get(0).getErrorType(), "invalid_auth");
    }

    //Test handling error response from server in case of missing or wrong version
    @Test
    public void testVersionMissingErrorCodeResponseFromFoursquareServer() throws Exception {
        String searchQuery = "oulu";

        String prepaidQuery = baseQuery + "client_id=" + clientID
                + "&client_secret=" + clientSecret + "&v="
                + "&ll=" + this.latitude + "," + this.longitude + "&query=" + searchQuery;//prepare query for server request
        String respFromServer = MyHttpManager.getData(prepaidQuery);//try to get data from server and return as string object contain JSON data
        VenueListJSONParsor jp = new VenueListJSONParsor();
        //parse json object into ArrayList and sent to presenter for further processing.
        ArrayList<VenueListDTO> venueListDTOs = jp.parseResp(respFromServer);

        // verify error code
        assertEquals(venueListDTOs.get(0).getErrorCode(), 400);
        // verify error type
        assertEquals(venueListDTOs.get(0).getErrorType(), "param_error");
    }

    //Test if JSONParser successfully parse the output from server and return it as ArrayList Object of VenueListDTOs
    @Test
    public void testJsonParserSuccessfulToParseContentReceiveFromServer() throws Exception {
        String searchQuery = "oulu";

        String prepaidQuery = baseQuery + "client_id=" + clientID
                + "&client_secret=" + clientSecret + "&v=" + apiVersion
                + "&ll=" + this.latitude + "," + this.longitude + "&query=" + searchQuery;//prepare query for server request
        String respFromServer = MyHttpManager.getData(prepaidQuery);//try to get data from server and return as string object contain JSON data
        VenueListJSONParsor jp = new VenueListJSONParsor();
        //parse json object into ArrayList and sent to presenter for further processing.
        ArrayList<VenueListDTO> venueListDTOs = jp.parseResp(respFromServer);
        assertTrue(!venueListDTOs.isEmpty());
    }

    //Test if JSONParser failed to parse the output from server
    @Test(expected = JSONException.class)
    public void testJsonParserFailedToParseContentReceiveFromServer() throws Exception {
        String searchQuery = "oulu";

        String prepaidQuery = "&query=" + searchQuery;//prepare query for server request
        String respFromServer = MyHttpManager.getData(prepaidQuery);//try to get data from server and return as string object contain JSON data
        VenueListJSONParsor jp = new VenueListJSONParsor();
        //parse json object into ArrayList and sent to presenter for further processing.
        ArrayList<VenueListDTO> venueListDTOs = jp.parseResp(respFromServer);
    }


    //Test if service is not able to make connection with foursquare server.
    @Test
    public void testHttpServiceFailedToMakeConnection() throws Exception {
        String searchQuery = "oulu";
        //here I will give the wrong query so HTTPClient will not be able to make connection
        String prepaidQuery = "&query=" + searchQuery;
        String respFromServer = MyHttpManager.getData(prepaidQuery);

        assertEquals(respFromServer, "Error in network connection");
    }

    // test if we are able to call server.
    @Test
    public void testHttpServerRequestIsReturningReponse() throws Exception {
        String searchQuery = "oulu";

        String prepaidQuery = baseQuery + "client_id=" + clientID
                + "&client_secret=" + clientSecret + "&v=" + apiVersion
                + "&ll=" + this.latitude + "," + this.longitude + "&query=" + searchQuery;
        String respFromServer = MyHttpManager.getData(prepaidQuery);

        assertTrue(!respFromServer.isEmpty());
    }

}