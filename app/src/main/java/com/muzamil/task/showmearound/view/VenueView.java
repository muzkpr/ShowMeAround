package com.muzamil.task.showmearound.view;

import android.content.Context;

import com.muzamil.task.showmearound.utility.VenueListDTO;

import java.util.ArrayList;

/**
 * Created by Muzamil on 21-Oct-15.
 */
public interface VenueView {
    void showSettingsAlert(String title, String message);

    void showResult(String result);

    void stopLocationManager();

    Context getSearchAppContext();

    void populateListView(ArrayList<VenueListDTO> venueListDTOs);

    void clearListView();

}
