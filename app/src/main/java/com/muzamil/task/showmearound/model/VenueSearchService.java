package com.muzamil.task.showmearound.model;

import android.os.AsyncTask;
import android.os.AsyncTask.Status;

import com.muzamil.task.showmearound.presenter.Presenter;
import com.muzamil.task.showmearound.utility.VenueListDTO;
import com.muzamil.task.showmearound.utility.VenueListJSONParsor;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Muzamil
 */
public class VenueSearchService {

    private String result;
    private Presenter presenter;

    public void search(Presenter presenter, String query) {


        this.presenter = presenter;
        // create AysncTask for HTTP request
        MyAsync myAsync = new MyAsync();
        //if any backgroud task is already running then interrupt it and start a new background task for the new query. usefull when user have slow connection and we are invoking http request on every character.
        if (myAsync.getStatus() == Status.RUNNING) {
            myAsync.cancel(true);
        }
        // delegate http request to background thread which will return response after completing the request.
        myAsync.execute(query);
    }

    private class MyAsync extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            //send http get request to foursqure and convert input stream from there as string.
            String respFromServer = MyHttpManager.getData(params[0]);


            return respFromServer;
        }

        @Override
        protected void onPostExecute(String result) {
            // if failed to make connection with server, send error message to presenter
            if (result.equalsIgnoreCase("Error in network connection")) {
                showResult(result);
            }
            //on getting response from server, pass it to presenter for further processing
            else {

                VenueListJSONParsor jp = new VenueListJSONParsor();
                //parse json object into ArrayList and sent to presenter for further processing.
                ArrayList<VenueListDTO> venueListDTOs = null;
                try {
                    venueListDTOs = jp.parseResp(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                showResponse(venueListDTOs);
            }

        }
    }

    //on getting response from server, pass it to presenter for furthure processing
    private void showResponse(ArrayList<VenueListDTO> venueListDTOs) {
        presenter.showResponse(venueListDTOs);
    }

    // if failed to make connection with server, send error message to presenter
    private void showResult(String result) {
        presenter.showResult(result);
    }
}
