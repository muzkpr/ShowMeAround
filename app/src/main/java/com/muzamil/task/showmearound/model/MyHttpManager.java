package com.muzamil.task.showmearound.model;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Muzamil.
 * HTTP manager to communicate with server using HttpURLConnection
 */
public class MyHttpManager {

    public static String getData(String uri) {

        BufferedReader reader = null;
        HttpURLConnection con = null;
        String errorInConnection = "Error in network connection";
        URL url;
        try {
            // create URL object from input uri
            url = new URL(uri);

            // Open HTTP url connection, HttpURLConnection uses GET method by default so we don't need to set method as get.
            con = (HttpURLConnection) url.openConnection();
            //Create String builder object to read input stream from server
            StringBuilder sb = new StringBuilder();
            int status = con.getResponseCode();
            //if server send bad request error
            if (status >= HttpURLConnection.HTTP_BAD_REQUEST) {
                // read the error response
                reader = new BufferedReader(new InputStreamReader(con.getErrorStream()));

            } else {
                // Read input stream from server
                reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            }

            String line;
            // read line by line the input stream
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            //convert and return string buffer as String
            return sb.toString();
        }
        // catch if any exception occur
        catch (IOException e) {
            e.printStackTrace();
            return errorInConnection;
        } finally {
            try {
                if (reader != null) {
                    reader.close();

                }
                if (con != null) {
                    con.disconnect();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
