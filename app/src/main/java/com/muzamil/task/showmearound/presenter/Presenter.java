package com.muzamil.task.showmearound.presenter;


import android.content.Context;

import com.muzamil.task.showmearound.model.GPSDataCollectionService;
import com.muzamil.task.showmearound.model.VenueSearchService;
import com.muzamil.task.showmearound.utility.VenueListDTO;
import com.muzamil.task.showmearound.view.VenueView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muzamil
 */
public class Presenter {
    private VenueView view;
    private VenueSearchService searchService;
    private String gpsSettingTitle = "GPS Settings";
    private String gpsSettingMessage = "GPS is not enabled. Do you want to go to settings menu?";
    private final String baseQuery = "https://api.foursquare.com/v2/venues/search?"; //base query string from foursquare
    private final String clientID = ""; //client id
    private final String clientSecret = ""; //client secret
    private final int apiVersion = 20130815; // foursquare api version
    private double latitude = 0.0; //latitude
    private double longitude = 0.0;// longitude
    private String searchQuery;
    private GPSDataCollectionService gpsDataCollectionService;


    public Presenter(VenueView view, VenueSearchService searchService) {
        this.view = view;
        this.searchService = searchService;
        gpsDataCollectionService = new GPSDataCollectionService(this.getSearchAppContext());

    }

    private Context getSearchAppContext() {
        return view.getSearchAppContext();
    }

    public void search(String query) {
        searchQuery = query;
        //check if GPS service is able to get location if false then show alert dialog to user so user can enable location services then call
        if (!gpsDataCollectionService.canGetLocation()) {

            showSettingsAlert(gpsSettingTitle, gpsSettingMessage);
        } else {
            // get latitude from GPS service
            latitude = gpsDataCollectionService.getLatitude();
            // get longitude from GPS service
            longitude = gpsDataCollectionService.getLongitude();
            // call search service and pass Presenter object to get response back and current geo positioning data
            searchService.search(this, buildQuery(searchQuery, latitude, longitude));
        }
    }

    // if GPS is disabled then show alert dialog to user so the user can enable it.
    private void showSettingsAlert(String title, String message) {
        view.showSettingsAlert(title, message);

    }

    //method to show error messages on activity display, such as network failure.
    public void showResult(String errorMessage) {

        view.showResult(errorMessage);
    }

    public void showResponse(List<VenueListDTO> venueListDTOs) {
        // in case of request failure we will get only one obeject in Array list so we can either show the response of call or can just show the general message
        if (!venueListDTOs.isEmpty()) {
            if (venueListDTOs.get(0).isRequestFailed()) {
                //if due to any network issue request get failed then we will show general error message, we can also pass DTO of error message which contains error_code, error_type, error_description.
                showResult("Connection with server failed, please try again later");
            } else {
                //populate list view on application view with venues received against user's query.
                view.populateListView((ArrayList<VenueListDTO>) venueListDTOs);

            }
        } else {
            // if foursqure api does not return anything in response to query then we will show this message to user
            showResult("No relevant venue found against your search string...");
            view.clearListView();

        }
        // search view is empty or user clear the search view, this method will clear Venue list on application view.
        if (searchQuery.isEmpty() || searchQuery.equals("")) {
            view.clearListView();
        }


    }

    // we will stop location manager on application kill, this method will be called at Activity.onDestory method
    public void stopLocationManager() {
        if (gpsDataCollectionService != null) {
            // kill the gps data collection service.
            gpsDataCollectionService.stopUsingGPS();
            gpsDataCollectionService.stopSelf();

        }
    }


    //build the search query to get data from server.
    public String buildQuery(String query, double latitude, double longitude) {
        // create query string
        String searchQuery = baseQuery + "client_id=" + clientID
                + "&client_secret=" + clientSecret + "&v=" + apiVersion
                + "&ll=" + this.latitude + "," + this.longitude + "&query=" + query;

        return searchQuery;
    }
}
