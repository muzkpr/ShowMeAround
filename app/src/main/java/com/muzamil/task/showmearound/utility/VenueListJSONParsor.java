package com.muzamil.task.showmearound.utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muzamil.
 * JSON parser to parse message received from sever.
 */
public class VenueListJSONParsor {


    // this method will receive json object as input and return a data transfer object (VenueListDTO)
    public ArrayList<VenueListDTO> parseResp(String content) throws JSONException {

        int code;
        String name;
        String address;
        int distance;


        List venueList = new ArrayList<VenueListDTO>();

        JSONObject jsonObj = new JSONObject(content);
        //response code from json object
        code = (((JSONObject) jsonObj.get("meta")).getInt("code"));
        // if we receive successful response from server.
        if (code == 200) {
            JSONArray jsonArray = ((JSONObject) jsonObj.get("response")).getJSONArray("venues");

            // check if response from server contains any message then set it as message string for VenueListDTO
            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.getJSONObject(i).has("name")) {
                    name = jsonArray.getJSONObject(i).getString("name");
                } else {
                    name = "name not received";
                }
                // check if response from server contains any secret then set it as secret value for VenueListDTO
                if (jsonArray.getJSONObject(i).getJSONObject("location").has("formattedAddress")) {
                    address = jsonArray.getJSONObject(i).getJSONObject("location").getString("formattedAddress");


                } else {
                    address = "address not received";

                }
                // check if response from server contains distan then set it as distance
                if (jsonArray.getJSONObject(i).getJSONObject("location").has("distance")) {
                    distance = (int) (jsonArray.getJSONObject(i).getJSONObject("location").getInt("distance"));

                } else {
                    distance = 0;
                }

                venueList.add(new VenueListDTO(name, distance, address));
            }

        } else {
            String errorType = (((JSONObject) jsonObj.get("meta")).getString("errorType")); // error type if response from server is not a success
            String errorDescription = (((JSONObject) jsonObj.get("meta")).getString("errorDetail"));//error details if response from server is not a success

            venueList.add(new VenueListDTO(true, code, errorType, errorDescription));

        }


        return (ArrayList<VenueListDTO>) venueList;
    }
}