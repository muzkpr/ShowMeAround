package com.muzamil.task.showmearound.utility;


/**
 * Created by Muzamil.
 * Data transfer object for message received from sever.
 */
public class VenueListDTO {
    private boolean requestFailed; //boolean to tell status of http call
    private int errorCode; // string to return error code if call is not successful
    private String errorType; // string to store the error type
    private String errorDescription; // string to store error description if call is not successful
    private String name; // string to store name if call is successful
    private String address;// string to store address if call is successful
    private String distance;// string to store distance if call is successful

    public VenueListDTO(boolean failure, int errorCode, String errorType, String errorDescription) {
        this.errorDescription = errorDescription;
        this.errorCode = errorCode;
        this.requestFailed = failure;
        this.errorType = errorType;
    }

    public VenueListDTO(String name, int distance, String address) {

        //perform check if name is empty
        if (name.isEmpty()) {
            name = "no name received";
        }
        this.name = name;

        //perform check if address is empty
        if (address == null) {
            address = "no address received";
        }
        this.address = address;
        // convert distance to kilometers if it is greater then 1000 meters
        if (distance < 1000)
            this.distance = distance + " Meters";
        else
            this.distance = distance * 0.001 + " Km";

    }

    //return error description
    public String getErrorDescription() {
        return errorDescription;
    }

    //return error code
    public int getErrorCode() {
        return errorCode;
    }

    //return error type
    public String getErrorType() {
        return errorType;
    }

    // return boolean to tell either call was successful or not
    public boolean isRequestFailed() {
        return requestFailed;
    }

    // return name
    public String getName() {
        return name;
    }

    // return address
    public String getAddress() {
        return address;
    }

    // return distance
    public String getDistance() {
        return distance;
    }


}
